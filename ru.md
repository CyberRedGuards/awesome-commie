# awesome-commie

Полезные ресурсы и программы для коммунистов.

Бесплатно, без рекламы, открытый код.

[Помощь](https://codeberg.org/CyberRedGuards/awesome-commie/src/branch/main/ru.md#%D0%BF%D0%BE%D0%BC%D0%BE%D1%89%D1%8C) по любым вопросам.

## Загрузка торрентов

[qBittorrent](https://www.fosshub.com/qBittorrent.html) - Windows.

[LibreTorrent](https://f-droid.org/ru/packages/org.proninyaroslav.libretorrent) - Android.

## Операционные системы

### Windows 10 установка и правильная настройка

Запись образа на USB:

[Rufus](https://github.com/pbatard/rufus/releases/latest) - Windows.

Загрузка, установка оригинальной версии и безопасная активация (с помощью скрипта):

[win10](https://codeberg.org/CyberRedGuards/win10)

[Контроль вредной активности](https://codeberg.org/CyberRedGuards/awesome-commie/src/branch/main/ru.md#%D0%BF%D0%BE%D0%BC%D0%BE%D1%89%D1%8C)

## Браузеры

Для повседневного использования:

[Iceweasel](https://sourceforge.net/projects/libportable/files/latest/download) - Windows.

[Fennec](https://f-droid.org/ru/packages/org.mozilla.fennec_fdroid/) - Android.

Для анонимного поиска информации:

[Tor Browser](https://www.torproject.org/dist/torbrowser/10.0.12/torbrowser-install-win64-10.0.12_ru.exe) - Windows.

### Дополнения

**uBlock** - блокировка рекламы, включить в дополнениях.

[TOSDR](https://addons.mozilla.org/ru-RU/firefox/addon/terms-of-service-didnt-read) - условия пользования и политика конфиденциальности (готовые оценки).

## Безопасное хранение паролей

[KeePassXC](https://keepassxc.org/download/#windows) - Windows.

[KeePassDX](https://f-droid.org/ru/packages/com.kunzisoft.keepass.libre/) - Android.

## Офис
[LibreOffice](https://download.documentfoundation.org/libreoffice/stable/7.1.1/win/x86_64/LibreOffice_7.1.1_Win_x64.msi.torrent) - Windows.

**Советы**: в настройках вида, можно сделать похожим на современные M$ Office.

## Чтение новостных лент

[QuiteRSS](https://quiterss.org/ru/download) - Windows.

[Подробнее](https://quiterss.org/ru/about)

[Feeder](https://f-droid.org/ru/packages/com.nononsenseapps.feeder/) - Android.

[Сборник лент](https://codeberg.org/CyberRedGuards/awesome-commie/raw/branch/main/awesome-commie.opml)

## Мониторинг социальных сетей

### Мониторинг VK

```
Внимание!

Приложение отправляет ваши данные о телефоне на сервера VK, как и оригинальное.
Не используйте его для полностью анонимного пользования!

Возможно в будущем будет исправлено, следите за обновлениями.
```

[Fenrir](https://github.com/umerov1999/Fenrir-for-VK/releases/latest) - Android.

Рекомендуем выбрать VK.

### Мониторинг Telegram

[Nekogram X](https://f-droid.org/ru/packages/nekox.messenger/) - Android.

### Мониторинг YouTube

[Invidious](https://github.com/iv-org/documentation/blob/master/Invidious-Instances.md#list-of-public-invidious-instances-sorted-from-oldest-to-newest) - браузер.

[MiniTube](https://flavio.tordini.org/files/minitube/minitube.exe) - Windows.

[NewPipe](https://f-droid.org/ru/packages/org.schabi.newpipe/) - Android.

### Мониторинг Twitter

[Nitter](https://github.com/zedeus/nitter/wiki/Instances) - браузер.

[Twidere](https://f-droid.org/ru/packages/org.mariotaku.twidere) - Android.

### Мониторинг Instagram

[Bibliogram](https://git.sr.ht/~cadence/bibliogram-docs/tree/master/docs/Instances.md) - браузер.

[Barinsta](https://f-droid.org/ru/packages/me.austinhuang.instagrabber/) - Android.

### Мониторинг Reddit

[Libreddit](https://github.com/spikecodes/libreddit#instances) - браузер.

### Skype, Discord и прочее

[Rambox](https://rambox.app/#ce) - Windows.

# Анти-консьюмеризм

## Интернет

[TrackerControl](https://f-droid.org/ru/packages/net.kollnig.missioncontrol.fdroid/) - Android.

[simplewall](https://github.com/henrypp/simplewall/releases/latest) - Windows.

## Магазин

[OpenFoodFacts](https://ru.openfoodfacts.org/) - Windows.

[OpenFoodFacts](https://f-droid.org/ru/packages/openfoodfacts.github.scrachx.openfood/) - Android.

[Подробнее](https://ru.openfoodfacts.org/discover)

Бонусные карты:

[Catima](https://f-droid.org/ru/packages/me.hackerchick.catima/) - Android.

## Телефон

### Xiaomi

[Xiaomi ADB/Fastboot Tools](https://github.com/Szaki/XiaomiADBFastbootTools/releases/latest) - для запуска требуется [Java](https://java.com/ru/download/manual.jsp).

[Инструкция](https://codeberg.org/CyberRedGuards/awesome-commie/src/branch/main/xiaomi_ru.md)

### Остальные

Прошивка [LineageOS](https://ru.wikipedia.org/wiki/LineageOS) является самой надёжной среди AOSP.

Операционная система [PosmarketOS](https://postmarketos.org), предусматривает возможность создания собственного порта.

[Помощь](https://codeberg.org/CyberRedGuards/awesome-commie/src/branch/main/ru.md#%D0%BF%D0%BE%D0%BC%D0%BE%D1%89%D1%8C)

# Игры

**Шахматы:**

[Lichess.org](https://lichess.org) - браузер.

[Lichess](https://android.izzysoft.de/repo/apk/org.lichess.mobileapp) / [скачать APK](https://android.izzysoft.de/frepo/org.lichess.mobileapp_7008010.apk) - Android.

**География:**

[GeoQuiz](https://f-droid.org/ru/packages/com.geoquizfoss/) - Android.

# Продуктивность

Контроль активности (Pomodoro):

[GetFlow](https://f-droid.org/ru/packages/org.wentura.getflow/) - Android.

Задачник:

[OpenTasks](https://f-droid.org/ru/packages/org.dmfs.tasks/) - Android.

# Прочее

Дни рождения:

[BirthDayDroid](https://f-droid.org/ru/packages/com.tmendes.birthdaydroid/) - Android.

# Помощь

[Публичный Jabber/XMPP чат](https://fedi.life/i/#%D0%B3%D0%BE%D0%BB%D0%BE%D1%81_%D0%BF%D1%80%D0%B0%D0%B2%D0%B4%D1%8B@conference.macaw.me?join).